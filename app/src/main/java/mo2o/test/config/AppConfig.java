package mo2o.test.config;

import mo2o.test.BuildConfig;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings({"WeakerAccess", "DefaultFileTemplate"})
public class AppConfig {

    //-------------------Network Settings---------------------------
    public static final int SOCKET_TIMEOUT_MS = 15000;
    public static final int MAX_RETRIES = 3;

    public static final String SERVER_BASE_API = BuildConfig.SERVER_BASE_API;
    public static final String SERVER_API_SEARCH = "?q=%s&p=%s";

    //-------------------Pagination Settings---------------------------
    public static final int LOADING_TRIGGER_THRESHOLD = 3;
}
