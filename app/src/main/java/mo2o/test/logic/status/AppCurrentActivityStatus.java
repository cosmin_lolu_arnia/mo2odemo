package mo2o.test.logic.status;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import java.lang.ref.WeakReference;

import mo2o.test.gui.base.BaseAppCompatActivity;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings("WeakerAccess")
public class AppCurrentActivityStatus implements Application.ActivityLifecycleCallbacks {

    private static class Loader {
        static volatile AppCurrentActivityStatus INSTANCE = new AppCurrentActivityStatus();
    }

    public static AppCurrentActivityStatus getInstance() {
        return AppCurrentActivityStatus.Loader.INSTANCE;
    }

    private WeakReference<Activity> activities;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (activity instanceof BaseAppCompatActivity) {
            activities = new WeakReference<>(activity);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (activity instanceof BaseAppCompatActivity) {
            activities = new WeakReference<>(activity);
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if (activity instanceof BaseAppCompatActivity) {
            activities = new WeakReference<>(activity);
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    public Activity getCurrentActivity() {
        if (activities != null) {
            return activities.get();
        } else {
            return null;
        }
    }
}