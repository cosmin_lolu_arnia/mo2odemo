package mo2o.test.logic.server.error;

import com.android.volley.VolleyError;

import mo2o.test.MO2OApp;
import mo2o.test.R;
import mo2o.test.gui.dialogs.AppDialogs;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class AppErrorManager {

    private static final String ERR_CONNECTION_1 = "com.android.volley.NoConnectionError";
    private static final String ERR_CONNECTION_2 = "No address associated with hostname";
    private static final String ERR_CONNECTION_3 = "Unable to resolve host";

    private static class Loader {
        static volatile AppErrorManager INSTANCE = new AppErrorManager();
    }

    public static AppErrorManager getInstance() {
        return Loader.INSTANCE;
    }

    public void processError(VolleyError error) {
        if (error != null &&error.networkResponse != null && error.networkResponse.statusCode == 500)
            return;

        if (error != null && error.getMessage() != null) {
            if (error.getMessage().contains(ERR_CONNECTION_1) || error.getMessage().contains(ERR_CONNECTION_2) || error.getMessage().contains(ERR_CONNECTION_3)) {
                AppDialogs.getInstance().connection(false).show();
            } else {
                AppDialogs.getInstance().error(MO2OApp.getInstance().getString(R.string.general_error_message)).show();
            }
        } else {
            AppDialogs.getInstance().error(MO2OApp.getInstance().getString(R.string.general_error_message)).show();
        }
    }
}
