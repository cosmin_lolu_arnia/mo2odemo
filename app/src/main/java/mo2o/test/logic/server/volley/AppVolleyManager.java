package mo2o.test.logic.server.volley;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import mo2o.test.MO2OApp;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings("WeakerAccess")
public class AppVolleyManager {

    private static AppVolleyManager instance;
    private RequestQueue requestQueue;

    private AppVolleyManager() {
        requestQueue = Volley.newRequestQueue(MO2OApp.getContext());
    }

    public static AppVolleyManager getInstance() {
        if (instance == null) {
            instance = new AppVolleyManager();
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }
}
