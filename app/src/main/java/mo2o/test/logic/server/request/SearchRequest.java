package mo2o.test.logic.server.request;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import mo2o.test.config.AppConfig;
import mo2o.test.logic.models.SearchResponse;


@SuppressWarnings("DefaultFileTemplate")
public class SearchRequest extends JsonRequest<SearchResponse> {

    private Response.Listener<SearchResponse> listener;

    public SearchRequest(String query, int page, Response.Listener<SearchResponse> listener, Response.ErrorListener errorListener) {
        super(Method.GET, AppConfig.SERVER_BASE_API + String.format(AppConfig.SERVER_API_SEARCH, query, page), null, listener, errorListener);
        this.listener = listener;
        LogUtils.d("Search Request: " + AppConfig.SERVER_BASE_API + String.format(AppConfig.SERVER_API_SEARCH, query, page));
    }

    @Override
    protected Response<SearchResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            LogUtils.i("Search Response: " + jsonString);
            SearchResponse data = new Gson().fromJson(jsonString, SearchResponse.class);
            return Response.success(data, null);
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(SearchResponse response) {
        listener.onResponse(response);
    }

}