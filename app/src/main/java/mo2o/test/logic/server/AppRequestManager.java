package mo2o.test.logic.server;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonRequest;

import mo2o.test.config.AppConfig;
import mo2o.test.logic.server.volley.AppVolleyManager;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings("WeakerAccess")
public class AppRequestManager {

    private static RequestQueue queue;

    private static AppRequestManager current;

    public static synchronized AppRequestManager getInstance() {
        if (current == null) {
            current = new AppRequestManager();
        }
        if (queue == null) {
            queue = AppVolleyManager.getInstance().getRequestQueue();
        }
        return current;
    }

    public void startRequest(Request request) {
        if (queue != null && request != null) {
            request.setRetryPolicy(new DefaultRetryPolicy(
                    AppConfig.SOCKET_TIMEOUT_MS,
                    AppConfig.MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(request);
        }
    }

    public void stopRequest(JsonRequest request) {
        if (queue != null && request != null) {
            queue.cancelAll(request);
        }
    }

    public RequestQueue getQueue() {
        return queue;
    }
}
