package mo2o.test.logic.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class SearchResponse {

    @SerializedName("title")
    private String title;

    @SerializedName("version")
    private double version;

    @SerializedName("href")
    private String href;

    @SerializedName("results")
    private List<Recipe> results;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<Recipe> getResults() {
        return results;
    }

    public void setResults(List<Recipe> results) {
        this.results = results;
    }
}
