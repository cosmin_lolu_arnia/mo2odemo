package mo2o.test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.blankj.utilcode.util.Utils;

import mo2o.test.logic.status.AppCurrentActivityStatus;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressLint("StaticFieldLeak")
public class MO2OApp extends Application {

    private static Context generalContext;
    private static MO2OApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        //init Utils Library
        Utils.init(this);

        //init general objects
        initObjects();

        //register app status
        registerAppStatus();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    private void initObjects() {
        if (generalContext == null) {
            generalContext = this.getApplicationContext();
        }

        if (instance == null) {
            instance = this;
        }
    }

    private void registerAppStatus() {
        registerActivityLifecycleCallbacks(AppCurrentActivityStatus.getInstance());
    }

    public static MO2OApp getInstance() {
        return instance;
    }

    public static Context getContext() {
        return generalContext;
    }

    public static Activity getActivity() {
        return AppCurrentActivityStatus.getInstance().getCurrentActivity();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
