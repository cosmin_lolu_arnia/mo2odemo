package mo2o.test.utils.image;

import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;

import mo2o.test.MO2OApp;
import mo2o.test.R;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings("WeakerAccess")
public class AppLoadImage {

    private static class Loader {
        static volatile AppLoadImage INSTANCE = new AppLoadImage();
    }

    public static AppLoadImage getInstance() {
        return Loader.INSTANCE;
    }

    public void imageLoader(final String url, final ImageView imageView, final boolean round) {
        if (imageView != null) {
            if (!round) {
                if (!TextUtils.isEmpty(url)) {
                    GlideApp.with(MO2OApp.getContext())
                            .load(url)
                            .placeholder(R.drawable.default_placeholder)
                            .error(R.drawable.default_placeholder)
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(imageView);

                } else {
                    GlideApp.with(MO2OApp.getContext())
                            .load(R.drawable.default_placeholder)
                            .placeholder(R.drawable.default_placeholder)
                            .error(R.drawable.default_placeholder)
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(imageView);
                }
            } else {
                if (!TextUtils.isEmpty(url)) {
                    GlideApp.with(MO2OApp.getContext())
                            .load(url)
                            .placeholder(R.drawable.default_round_placeholder)
                            .error(R.drawable.default_round_placeholder)
                            .apply(bitmapTransform(new CircleCrop()))
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(imageView);
                } else {
                    GlideApp.with(MO2OApp.getContext())
                            .load(R.drawable.default_round_placeholder)
                            .placeholder(R.drawable.default_round_placeholder)
                            .error(R.drawable.default_round_placeholder)
                            .apply(bitmapTransform(new CircleCrop()))
                            .format(DecodeFormat.PREFER_RGB_565)
                            .into(imageView);
                }
            }
        }
    }
}
