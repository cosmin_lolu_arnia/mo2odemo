package mo2o.test.utils.gson;

import com.google.gson.Gson;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings({"SpellCheckingInspection", "WeakerAccess"})
public class AppGson {

    private Gson gson;

    public AppGson() {
        this.gson = new Gson();
    }

    private static class Loader {
        static volatile AppGson INSTANCE = new AppGson();
    }

    public static AppGson getInstance() {
        return Loader.INSTANCE;
    }

    public String objectToJson(Object object) {
        return gson.toJson(object);
    }

    public Gson getGson() {
        return gson;
    }
}
