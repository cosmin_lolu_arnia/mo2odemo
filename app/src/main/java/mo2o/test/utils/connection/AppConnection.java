package mo2o.test.utils.connection;

import com.blankj.utilcode.util.NetworkUtils;

import mo2o.test.gui.dialogs.AppDialogs;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class AppConnection {

    public static boolean hasConnection(final boolean closeApp) {
        if (!NetworkUtils.isConnected()) {
            AppDialogs.getInstance().connection(closeApp).show();
            return false;
        }
        return true;
    }
}
