package mo2o.test.gui.screens.recipe;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mo2o.test.R;
import mo2o.test.gui.base.BaseAppCompatActivity;
import mo2o.test.logic.models.Recipe;
import mo2o.test.utils.gson.AppGson;
import mo2o.test.utils.image.AppLoadImage;
import mo2o.test.utils.send.AppExtraIntent;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class RecipeDetailsScreen extends BaseAppCompatActivity {

    @BindView(R.id.recipe_image)
    ImageView recipeImage;

    @BindView(R.id.recipe_title)
    AppCompatTextView recipeTitle;

    @BindView(R.id.recipe_ingredients)
    AppCompatTextView recipeIngredients;

    @BindView(R.id.recipe_web)
    AppCompatTextView recipeWeb;

    private Recipe recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_details_screen);
        ButterKnife.bind(this);

        getBundleExtra();
        initActionbar();
        addRecipeInfo();
    }

    private void getBundleExtra() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String jsonRes = extras.getString(AppExtraIntent.EXTRA_RECIPE);
            recipe = AppGson.getInstance().getGson().fromJson(jsonRes, Recipe.class);
        }
    }

    private void initActionbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
        }
    }

    private void addRecipeInfo() {
        if (recipe != null) {
            AppLoadImage.getInstance().imageLoader(recipe.getThumbnail(), recipeImage, false);
            recipeTitle.setText(String.valueOf(recipe.getTitle()));
            recipeIngredients.setText(String.valueOf(recipe.getIngredients()));
            recipeWeb.setText(String.valueOf(recipe.getHref()));
        }
    }
}
