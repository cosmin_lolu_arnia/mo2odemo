package mo2o.test.gui.screens.search.control;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public interface IOnSearchQuery {

    void onQuery(String query);
}
