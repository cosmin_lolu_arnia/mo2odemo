package mo2o.test.gui.screens.search.control;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.support.v7.widget.SearchView;

import mo2o.test.MO2OApp;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class SearchControl {

    private IOnSearchQuery iOnSearchQuery;

    private static class SingletonHolder {
        @SuppressLint("StaticFieldLeak")
        private static final SearchControl INSTANCE = new SearchControl();
    }

    public static SearchControl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public SearchControl init(SearchView searchView) {
        if (searchView != null) {
            SearchManager searchManager = (SearchManager) MO2OApp.getContext().getSystemService(Context.SEARCH_SERVICE);
            if (searchManager != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(MO2OApp.getActivity().getComponentName()));
            }
            addOnSearchQueryListener(searchView);
        }
        return this;
    }

    private void addOnSearchQueryListener(SearchView searchView) {
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (iOnSearchQuery != null) {
                        iOnSearchQuery.onQuery(query);
                    }
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (iOnSearchQuery != null) {
                        iOnSearchQuery.onQuery(newText);
                    }
                    return false;
                }
            });
        }
    }

    public void registerSearchQueryListener(IOnSearchQuery iOnSearchQuery) {
        this.iOnSearchQuery = iOnSearchQuery;
    }
}
