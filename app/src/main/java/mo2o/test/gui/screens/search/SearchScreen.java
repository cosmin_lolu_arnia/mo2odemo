package mo2o.test.gui.screens.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.blankj.utilcode.util.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mo2o.test.MO2OApp;
import mo2o.test.R;
import mo2o.test.config.AppConfig;
import mo2o.test.gui.base.BaseAppCompatActivity;
import mo2o.test.gui.screens.recipe.RecipeDetailsScreen;
import mo2o.test.gui.screens.search.adapter.SearchAdapter;
import mo2o.test.gui.screens.search.control.IOnSearchQuery;
import mo2o.test.gui.screens.search.control.SearchControl;
import mo2o.test.gui.view.recylerview.GridSpacingItemDecoration;
import mo2o.test.gui.view.recylerview.IOnItemClickListener;
import mo2o.test.logic.models.Recipe;
import mo2o.test.logic.models.SearchResponse;
import mo2o.test.logic.server.AppRequestManager;
import mo2o.test.logic.server.error.AppErrorManager;
import mo2o.test.logic.server.request.SearchRequest;
import mo2o.test.utils.gson.AppGson;
import mo2o.test.utils.send.AppExtraIntent;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class SearchScreen extends BaseAppCompatActivity implements IOnSearchQuery {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private SearchRequest searchRequest;
    private String query;
    private List<Recipe> items;
    private SearchAdapter adapter;
    private boolean isLoadingData;
    private SearchResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_screen);
        ButterKnife.bind(this);

        initActionbar();
        initItems();
        addOnItemClick();
        addOnScrollListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        SearchControl.getInstance().init(searchView).registerSearchQueryListener(this);
        return true;
    }

    private void initActionbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setActionBarWithoutIndicator(toolbar);
    }

    private void initItems() {
        this.items = new ArrayList<>();
        this.adapter = new SearchAdapter(items);
        if (ScreenUtils.isPortrait()) {
            this.recyclerView.setLayoutManager(new GridLayoutManager(MO2OApp.getContext(), 1));
            this.recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, 5, true));
            this.recyclerView.setAdapter(adapter);
        } else {
            this.recyclerView.setLayoutManager(new GridLayoutManager(MO2OApp.getContext(), 2));
            this.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, 5, true));
            this.recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onQuery(String query) {
        this.query = query;
        resetDataForeRefresh(items, adapter);
        if (!TextUtils.isEmpty(query)) {
            addSearchProcess(query);
        } else {
            checkForResponseData(items);
        }
    }

    private void addSearchProcess(String query) {
        isLoadingData = true;
        searchRequest = new SearchRequest(query, currentPage, new Response.Listener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse responseData) {
                response = responseData;
                if (responseData != null && responseData.getResults() != null) {
                    items.addAll(responseData.getResults());
                }

                populateData();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                response = null;
                isLoadingData = false;
                AppErrorManager.getInstance().processError(volleyError);
            }
        });
        AppRequestManager.getInstance().startRequest(searchRequest);
    }

    private void populateData() {
        if (recyclerView.getAdapter() == null) {
            adapter = new SearchAdapter(items);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.update(items);
        }
        isLoadingData = false;
        checkForResponseData(items);
    }

    private void addOnItemClick() {
        if (adapter != null) {
            adapter.setOnItemClickListener(new IOnItemClickListener() {
                @Override
                public void onItemClick(Object object) {
                    Intent intent = new Intent(SearchScreen.this, RecipeDetailsScreen.class);
                    intent.putExtra(AppExtraIntent.EXTRA_RECIPE, AppGson.getInstance().objectToJson(object));
                    startActivity(intent);
                }
            });
        }
    }

    private void addOnScrollListener() {
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
                    int lastVisibleItem = manager.findLastVisibleItemPosition();
                    if (lastVisibleItem >= manager.getItemCount() - AppConfig.LOADING_TRIGGER_THRESHOLD && canLoadNextPage() && !isLoadingData) {
                        currentPage++;
                        addSearchProcess(query);
                    }
                }
            });
        }
    }

    private boolean canLoadNextPage() {
        return !(items == null || response == null || response.getResults() == null || response.getResults().isEmpty() || TextUtils.isEmpty(query));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppRequestManager.getInstance().stopRequest(searchRequest);
    }
}
