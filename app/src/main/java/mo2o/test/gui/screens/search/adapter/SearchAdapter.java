package mo2o.test.gui.screens.search.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mo2o.test.R;
import mo2o.test.gui.view.recylerview.IOnItemClickListener;
import mo2o.test.logic.models.Recipe;
import mo2o.test.utils.image.AppLoadImage;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ResourceHolder> {

    private List<Recipe> items;
    private IOnItemClickListener onItemClickListener;

    public SearchAdapter(List<Recipe> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    @Override
    public ResourceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item_row, parent, false);
        return new ResourceHolder(view);
    }

    @Override
    public void onBindViewHolder(ResourceHolder holder, int position) {
        Recipe resource = items.get(position);
        holder.bind(resource, onItemClickListener);
    }

    @SuppressWarnings("WeakerAccess")
    protected class ResourceHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_image)
        ImageView itemImage;

        @BindView(R.id.item_title)
        AppCompatTextView itemTile;

        @BindView(R.id.item_sub_title)
        AppCompatTextView itemSubTitle;

        public ResourceHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(@Nullable final Recipe recipe, @Nullable final IOnItemClickListener onItemClickListener) {
            if (recipe != null) {
                AppLoadImage.getInstance().imageLoader(recipe.getThumbnail(), itemImage, true);
                itemTile.setText(String.valueOf(recipe.getTitle()));
                itemSubTitle.setText(String.valueOf(recipe.getIngredients()));
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(recipe);
                        }
                    }
                });
            }
        }
    }

    public void update(List<Recipe> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(@Nullable final IOnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
