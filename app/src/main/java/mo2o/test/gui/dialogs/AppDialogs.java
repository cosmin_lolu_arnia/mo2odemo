package mo2o.test.gui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;

import mo2o.test.MO2OApp;
import mo2o.test.R;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings("WeakerAccess")
public class AppDialogs {
    private static class Loader {
        static volatile AppDialogs INSTANCE = new AppDialogs();
    }

    public static AppDialogs getInstance() {
        return Loader.INSTANCE;
    }

    private AlertDialog.Builder alertBuilder;
    private AlertDialog alertDialog;

    //-------------------Init and Control part--------------------------------
    public AppDialogs withView(View view) {
        if (view != null && this.alertBuilder != null) {
            this.alertBuilder.setView(view);
        }
        return this;
    }

    public AppDialogs withPositiveButtonListener(String label, DialogInterface.OnClickListener listener) {
        if (listener != null && this.alertBuilder != null) {
            this.alertBuilder.setPositiveButton(label, listener);
        }
        return this;
    }

    public AppDialogs withNegativeButtonListener(String label, DialogInterface.OnClickListener listener) {
        if (listener != null && this.alertBuilder != null) {
            this.alertBuilder.setNegativeButton(label, listener);
        }
        return this;
    }

    public AppDialogs withNeutralButtonListener(String label, DialogInterface.OnClickListener listener) {
        if (listener != null && this.alertBuilder != null) {
            this.alertBuilder.setNeutralButton(label, listener);
        }
        return this;
    }

    public void show() {
        if (this.alertBuilder != null && !isAlertDialogVisible()) {
            this.alertDialog = alertBuilder.create();
            this.alertDialog.setCanceledOnTouchOutside(false);
            this.alertDialog.setCancelable(false);
            if (MO2OApp.getActivity() != null && !MO2OApp.getActivity().isFinishing()) {
                this.alertDialog.show();
                keepDialogOnRotateState(alertDialog);
            }
        }
    }

    //--------------------Dialogs---------------------------------------------
    public AppDialogs general(String title, String message) {
        if (MO2OApp.getActivity() != null && !MO2OApp.getActivity().isFinishing() && !isAlertDialogVisible()) {
            alertBuilder = new AlertDialog.Builder(MO2OApp.getActivity(), R.style.AlertDialogStyle);
            alertBuilder.setTitle(title);
            alertBuilder.setMessage(message);
        }
        return this;
    }

    public AppDialogs error(String message) {
        if (MO2OApp.getActivity() != null && !MO2OApp.getActivity().isFinishing() && !isAlertDialogVisible()) {
            alertBuilder = new AlertDialog.Builder(MO2OApp.getActivity(), R.style.AlertDialogStyle);
            alertBuilder.setTitle(MO2OApp.getContext().getString(R.string.general_error_title));
            alertBuilder.setMessage(message);
            alertBuilder.setPositiveButton(MO2OApp.getContext().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        return this;
    }

    public AppDialogs info(String message) {
        if (MO2OApp.getActivity() != null && !MO2OApp.getActivity().isFinishing() && !isAlertDialogVisible()) {
            alertBuilder = new AlertDialog.Builder(MO2OApp.getActivity(), R.style.AlertDialogStyle);
            alertBuilder.setTitle(MO2OApp.getContext().getString(R.string.general_info_title));
            alertBuilder.setMessage(message);
            alertBuilder.setPositiveButton(MO2OApp.getContext().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        return this;
    }

    public AppDialogs connection(final boolean closeApp) {
        if (MO2OApp.getActivity() != null && !MO2OApp.getActivity().isFinishing() && !isAlertDialogVisible()) {
            alertBuilder = new AlertDialog.Builder(MO2OApp.getActivity(), R.style.AlertDialogStyle);
            alertBuilder.setTitle(MO2OApp.getContext().getString(R.string.conn_down_title));
            alertBuilder.setMessage(MO2OApp.getContext().getString(R.string.conn_down_message));
            alertBuilder.setPositiveButton(MO2OApp.getContext().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (closeApp) {
                        MO2OApp.getActivity().finishAffinity();
                    }
                }
            });
        }
        return this;
    }

    //-------------------Util Part--------------------------------

    @SuppressWarnings("ConstantConditions")
    private void keepDialogOnRotateState(Dialog dialog) {
        if (dialog != null) {
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setAttributes(lp);
        }
    }

    private boolean isAlertDialogVisible() {
        return alertDialog != null && alertDialog.isShowing();
    }

    public void destroyDialogInstance() {
        alertBuilder = null;
        alertDialog = null;
    }

    public AlertDialog getAlertDialog() {
        return alertDialog;
    }
}