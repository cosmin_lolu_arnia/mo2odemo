package mo2o.test.gui.view.recylerview;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

public interface IOnItemClickListener {

    void onItemClick(Object object);
}
