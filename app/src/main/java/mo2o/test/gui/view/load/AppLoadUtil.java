package mo2o.test.gui.view.load;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ProgressBar;

import com.blankj.utilcode.util.LogUtils;

import mo2o.test.MO2OApp;
import mo2o.test.R;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressWarnings("deprecation")
public class AppLoadUtil {

    private static class Loader {
        static volatile AppLoadUtil INSTANCE = new AppLoadUtil();
    }

    public static AppLoadUtil getInstance() {
        return Loader.INSTANCE;
    }

    private boolean loadIsShowing;
    private ProgressDialog loading;

    @SuppressWarnings("deprecation")
    public void showProgressDialog(String message) {
        if (MO2OApp.getActivity() != null && !MO2OApp.getActivity().isFinishing()) {
            try {
                if (!loadIsShowing) {
                    loadIsShowing = true;
                    loading = new ProgressDialog(MO2OApp.getActivity());
                    Drawable drawable = ContextCompat.getDrawable(MO2OApp.getContext(), R.drawable.progressbar_color);
                    loading.setIndeterminateDrawable(drawable);
                    if (message != null) {
                        loading.setMessage(message);
                    }
                    loading.setCancelable(false);
                    loading.setIndeterminate(false);
                    loading.show();
                }
            } catch (Exception ex) {
                LogUtils.e(ex.getMessage());
            }
        }
    }

    public void hideProgressDialog() {
        if (loading != null && loadIsShowing) {
            loadIsShowing = false;
            try {
                loading.cancel();
            } catch (Exception ex) {
                LogUtils.e(ex.getMessage());
            }
        } else {
            loadIsShowing = false;
        }
    }

    //----------------------Big Load-----------------------

    public void showBigLoad(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progress_load);
            if (progressBar != null && progressBar.getVisibility() != View.VISIBLE) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    public void hideBigLoad(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progress_load);
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    public void showBigLoad(View rootView) {
        if (rootView != null) {
            ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progress_load);
            if (progressBar != null && progressBar.getVisibility() != View.VISIBLE) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    public void hideBigLoad(View rootView) {
        if (rootView != null) {
            ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progress_load);
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    //----------------------No Data View-----------------------

    public void showLoadNoData(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null && !fragment.getActivity().isFinishing() && fragment.getView() != null) {
            AppCompatTextView noDataTxt = fragment.getView().findViewById(R.id.no_data);
            if (noDataTxt != null) {
                noDataTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    public void hideLoadNoData(Fragment fragment) {
        if (fragment != null && fragment.getActivity() != null && !fragment.getActivity().isFinishing() && fragment.getView() != null) {
            AppCompatTextView noDataTxt = fragment.getView().findViewById(R.id.no_data);
            if (noDataTxt != null) {
                noDataTxt.setVisibility(View.GONE);
            }
        }
    }

    public void showLoadNoData(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AppCompatTextView noDataTxt = activity.findViewById(R.id.no_data);
            if (noDataTxt != null) {
                noDataTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    public void hideLoadNoData(Activity activity) {
        if (activity != null && !activity.isFinishing()) {
            AppCompatTextView noDataTxt = activity.findViewById(R.id.no_data);
            if (noDataTxt != null) {
                noDataTxt.setVisibility(View.GONE);
            }
        }
    }
}
