package mo2o.test.gui.base;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.List;

import mo2o.test.MO2OApp;
import mo2o.test.gui.view.load.AppLoadUtil;

/**
 * Created by cosmin on 11/22/2017.
 * Android Developer
 * cosmin.lolu@arnia.ro
 */

@SuppressLint("Registered")
public class BaseAppCompatActivity extends AppCompatActivity {

    public int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void setActionBar(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
            }
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            addToolbarTopMargin(toolbar);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public void setActionBarWithoutIndicator(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(false);
                actionBar.setDisplayShowHomeEnabled(false);
            }
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            addToolbarTopMargin(toolbar);
        }
    }

    public void addToolbarTopMargin(Toolbar toolbar) {
        if (toolbar == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ViewGroup.LayoutParams params = toolbar.getLayoutParams();
            int paddingPixel = 24;
            float density = MO2OApp.getContext().getResources().getDisplayMetrics().density;
            int statusBarHeight = (int) (paddingPixel * density);
            if (params instanceof CoordinatorLayout.LayoutParams) {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) params;
                lp.setMargins(0, statusBarHeight, 0, 0);
            } else if (params instanceof CollapsingToolbarLayout.LayoutParams) {
                CollapsingToolbarLayout.LayoutParams lp = (CollapsingToolbarLayout.LayoutParams) params;
                lp.setMargins(0, statusBarHeight, 0, 0);
            } else if (params instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) params;
                lp.setMargins(0, statusBarHeight, 0, 0);
            } else if (params instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) params;
                lp.setMargins(0, statusBarHeight, 0, 0);
            } else if (params instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) params;
                lp.setMargins(0, statusBarHeight, 0, 0);
            }
        }
    }

    public void resetDataForeRefresh(List list, RecyclerView.Adapter adapter) {
        currentPage = 1;

        if (list != null && !list.isEmpty()) {
            list.clear();
        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void checkForResponseData(List items) {
        if (items == null) {
            AppLoadUtil.getInstance().showLoadNoData(this);
        } else {
            if (items.isEmpty()) {
                AppLoadUtil.getInstance().showLoadNoData(this);
            } else {
                AppLoadUtil.getInstance().hideLoadNoData(this);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
